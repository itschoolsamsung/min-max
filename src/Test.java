public class Test {
    public static void main(String[] args) {
        int[] arr = new int[10];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int)(Math.random() * 10);
            System.out.printf("%d ", arr[i]);
        }

        int min = arr[0];
        int max = arr[0];

        for (int x : arr) {
            if (x < min) {
                min = x;
            }
            if (x > max) {
                max = x;
            }
        }

        System.out.printf("\nmin=%d, max=%d", min, max);
    }
}
